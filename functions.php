<?php

/*CSS3 PIE
This is included here as a format reminder, as it's identical to what's in the parent theme.
If you need to CSS3PIE something in your project, try to apply the class .css3_pie to that
element in the markup.  Failing that, add the appropriate selector to the jQuery below and
uncomment the call to add action.*/


function lxb_base_child_css3_pie(){
?>
	<!--[if lt IE 10]>
	<script>
		jQuery(function() {
	    	if (window.PIE) {
	    	    jQuery('*** YOUR SELECTOR HERE ***').each(function() {
			         PIE.attach(this);
			    });
			}
		});
	</script>
	<![endif]-->
<?php
}

//add_action('wp_footer', 'lxb_base_child_css3_pie');



function lxb_base_print_screen(){
	
	$print=esc_html__('Print', 'starkers');
	
	$out ='<span class="post-print"><a href="javascript:window.print()"><i class="icon-print"></i></a></span>';
		
	return $out;
}


function lxb_custom_print_style() {
wp_enqueue_style( 'twentytwelve-style', get_stylesheet_uri() );
wp_enqueue_style( 'lxb-base-print-style', get_stylesheet_directory_uri().'/print.css', '', '', 'print');
}

add_action( 'wp_enqueue_scripts', 'lxb_custom_print_style' );




function lxb_base_comments_link(){
		
	global $post;
		
	if ( comments_open() ) {
			
		comments_popup_link( 
   			__( "<i class='icon-comment'></i>", 'twentytwelve' ), 
   			__( "<i class='icon-comment'></i>", 'twentytwelve' ), 
   			__( "<i class='icon-comment'></i>", 'twentytwelve' ),
   			'post-comments',
   			__( 'Comments are off for this post', 'twentytwelve' )
		);
		
	}
}


function lxb_icon_menu($atts) {
	extract( shortcode_atts( array(
		'menu' => 'Global Navigation',
		'id' => 'site-navigation',
		'class' => 'main-navigation',
		'toggle' => 1,
		'toggle_icon' => "icon-double-angle-down",
	), $atts ) );
	
	if(empty($toggle_label)){
		$toggle_label="MENU";
	}
	
	if(!empty($toggle_icon)){
		$toggle_icon="<i class='$toggle_icon'></i>";
	}
	
	if(!empty($toggle)){
		$toggle="<h3 class='menu-toggle'>$toggle_icon</h3>";
	}
	
	$id= sanitize_html_class($id);
	$class= sanitize_html_class($class);
	
	$out="
		<nav id='$id' class='$class' role='navigation'>
			$toggle".
			wp_nav_menu( array( 'menu' => "$menu", 'menu_class' => 'nav-menu', 'echo' => false ) )."
		</nav>
	";
	
	$out = str_replace('<a href="#">###search###</a>', lxb_base_get_search_form(), $out);
	$out = str_replace('<a href="#">###firm###</a>', lxb_base_get_firm_link(), $out);
	
	return $out;
	
}
add_shortcode('lxb_icon_menu', 'lxb_icon_menu');





function lxb_base_child_trash_unused_sidebars(){
    //unregister_sidebar( 'footer-extras' );
	//unregister_sidebar( 'footer-contact' );
    //unregister_sidebar( 'sidebar-1' );
	//unregister_sidebar( 'colophon' );
	//unregister_sidebar( 'sub-colophon' );

}    
//add_action('init', 'lxb_base_child_trash_unused_sidebars');


function lxb_base_fix_some_stuff(){
	?>
	<script>		
	
		jQuery(document).ready(function() {
			
			<?php /* WPCF7: remove default text from form fields when clicked */ ?>
			jQuery('input[type="text"], input[type="email"]').focus(function() {
				if (this.value == this.defaultValue){
					this.value = '';
				}
				if(this.value != this.defaultValue){
					this.select();
				}
			});
	
			jQuery('input[type="text"], input[type="email"]').blur(function() {
				if (this.value == ''){
					this.value = this.defaultValue;
				}
			});

			jQuery('textarea').focus(function() {
				if (this.value == this.defaultValue){
					this.value = '';
				}
				if(this.value != this.defaultValue){
					this.select();
				}
			});

			jQuery('textarea').blur(function() {
				if (this.value == ''){
					this.value = this.defaultValue;
				}
			});
		
		
		
			<?php /*add a class for 'parent' list items in link lists*/ ?>
			jQuery('.links ul').parent().addClass('links_parent');



			<?php /* style tables */ ?>
			jQuery('.post-content table tr:even').addClass('even row');
			jQuery('.post-content table tr:odd').addClass('odd row');
			
			
			
			<?php /* menu toggle */ ?>
			jQuery('.menu-toggle').click(function(){
				jQuery(this).next('div').slideToggle();			
			});
			
			<?php /* make sure blockquotes and uls clear floated images */ ?>
			var blockquotes = jQuery('.post-content .alignleft, .post-content .alignright').nextAll('blockquote, ul, ol, dl');
			jQuery('<div class="pad-div" style="clear: both; padding-top: 20px"></div>').insertBefore(blockquotes);
			
		});
		
	</script>
	<?php
	}




	function lxb_base_fix_some_stuff_on_load(){
	?>
	<script>		
	
		jQuery( window ).load(function() {

			<?php /* make sidebar min-height of main column */ ?>	
			var main_height = jQuery('.main-wrapper .main').height() + 90;
			var sidebar_height = jQuery('.main-wrapper .sidebar').height() +40;
			jQuery('.main-wrapper .sidebar').css('min-height', main_height);
			jQuery('.main-wrapper .main').css('min-height', sidebar_height);
			
			
		});	
			
		
	</script>
	<?php
	}

add_action('wp_footer', 'lxb_base_fix_some_stuff_on_load');





//set the primary breakpoint width
function lxb_base_primary_breakpoint(){
	return "800px";
}










// call the correct typekit
function lxb_base_child_typekit(){?>

<script src="//use.typekit.net/ffz7bzs.js"></script>
<script>try{Typekit.load();}catch(e){}</script>
<?php	
}
add_action('wp_head', 'lxb_base_child_typekit');








	function lxb_base_alt_search_form( ){
		$search_text = esc_attr__("Enter Terms", "twentytwelve");
		$form='
			<form method="get" class="searchform" action="'.esc_url(home_url()).'/">
				<input type="text" value="'.$search_text.'" name="s" class="s"/>
			</form>
		';
		return $form;
	}

add_shortcode('lxb_base_alt_search_form', 'lxb_base_alt_search_form');



?>