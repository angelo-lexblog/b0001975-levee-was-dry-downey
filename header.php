<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

<!DOCTYPE html>

<?php if(function_exists('lxb_base_html_id')){lxb_base_html_id();} ?>

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	
	<?php wp_head(); ?>
	
	<!--[if lt IE 10]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/PIE.js"></script>
	<![endif]-->
	
</head>

<body <?php body_class(); ?>>

<?php //print only ?>
<?php if(function_exists('lxb_base_print_header')){echo lxb_base_print_header();} ?>

<div id="page" class="hfeed css3_pie blog-wrapper">
	
	<?php if(is_active_sidebar('super-header-first') || is_active_sidebar('super-header-second')){ ?>
		<div class='super_header'>
			<div class='super_header_inner_wrapper'>
				
				<?php if(is_active_sidebar('super-header-first')){ ?>
				<div class='super_header_first'>
					<?php if(dynamic_sidebar( 'super-header-first' ) ); ?>
				</div>
				<?php } ?>

				<?php if(is_active_sidebar('super-header-second')){ ?>
				<div class='super_header_second'>
					<?php if(dynamic_sidebar( 'super-header-second' ) ); ?>
				</div>
				<?php } ?>
			<div class="clear_div"></div>
			</div>
		</div>
	<?php } ?>
	
	<div class='clear_div'></div>
	
	<header id="masthead" class="site-header blog-header" role="banner">
		<div class='blog_header_inner_wrapper'>
			<?php if(dynamic_sidebar('header')); ?>
		</div>		
	</header><!-- #masthead -->

	<div class="wrapper main-wrapper">
	
		<div class="main<?php if(is_archive() ) { echo ' grid'; } ?> ">
		
		