<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>

</div><!-- .main -->

<?php get_sidebar(); ?>

<div class="clear_div"></div>

<div class="footer_blog_title"><a href="/"><?php echo bloginfo('title');?></a></div>

</div><!-- .mainwrapper -->

	<footer id="blog-footer" class="blog-footer">


		<div id="blog-footer-inner-wrapper" class="blog-footer-inner-wrapper">
		
			<?php if ( is_active_sidebar( 'footer-contact' ) ) : ?>
				<section class="footer-contact" role="complementary">
					<?php dynamic_sidebar( 'footer-contact' ); ?>
				</section>
			<?php endif; ?>

		
			<?php if ( is_active_sidebar( 'footer-extras' ) ) : ?>
				<section class="footer-extras" role="complementary">
					<?php dynamic_sidebar( 'footer-extras' ); ?>
				</section>
			<?php endif; ?>	
		
		</div>
	
	</footer>
	
		<?php if ( is_active_sidebar( 'colophon' ) ) : ?>
		<section class="colophon" role="complementary">
			<div class="colophon-inner-wrapper">
				<?php dynamic_sidebar( 'colophon' ); ?>
			</div>
		</section>
	<?php endif; ?>	
	

	
	<?php if ( is_active_sidebar( 'sub-colophon' ) ) : ?>
		<section class="sub-colophon" role="complementary">
			<div class="sub-colophon-inner-wrapper">
				<?php dynamic_sidebar( 'sub-colophon' ); ?>
			</div>
		</section>
	<?php endif; ?>	
	
	<?php echo lxb_base_footer_print_logo(); ?>

	
</div><!-- close wrapper div started in header.php -->

<?php echo lxb_base_browser_update(); ?>

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
	
</body>
</html>