<?php
/**
 * The template for displaying Archive pages.
 *
 * @package WordPress
 * @subpackage twentytwelve
 * @since twentytwelve HTML5 3.0
 */

get_header(); ?>


		<header id="archive-header" class="archive-header">
			<h1 id="archive-title" class="archive-title">
<?php if ( is_day() ) : ?>
				<?php printf( __( 'Daily Archives: <strong>%s</strong>', 'twentytwelve' ), get_the_date() ); ?>
<?php elseif ( is_month() ) : ?>
				<?php printf( __( 'Monthly Archives: <strong>%s</strong>', 'twentytwelve' ), get_the_date('F Y') ); ?>
<?php elseif ( is_year() ) : ?>
				<?php printf( __( 'Yearly Archives: <strong>%s</strong>', 'twentytwelve' ), get_the_date('Y') ); ?>
<?php elseif ( is_tag() ) : ?>
				<?php printf( __( 'Tag: <strong>%s</strong>', 'twentytwelve' ), single_tag_title( '', false ) ); ?>
<?php elseif ( is_category() ) : ?>
				<?php printf( __( 'Category: <strong>%s</strong>', 'twentytwelve' ), single_cat_title( '', false ) ); ?>
<?php else : ?>
				<?php _e( 'Archives', 'twentytwelve' ); ?>
<?php endif; ?>
			</h1>
			
			<?php
			if ( is_archive() && is_category() ) { // if this is a cat archive page
				if ( category_description( get_query_var('cat') ) ) {
		        	echo '<div class="cat_description">'.category_description( get_query_var('cat') ).'</div>';
				}
    		}
			?>

			
			
			<?php if ( is_category() ) : // if the archive is for a category, show a link to the feed ?>
				<?php
					$this_category = get_category($cat);// This line just gets the active category information
					echo '<a class="feed" href="'.get_category_feed_link($this_category->cat_ID, '').'">';
					echo '<i class="icon-rss"></i>';
					printf( __( 'Subscribe to %s RSS Feed', 'twentytwelve' ), single_cat_title( '', false ) );
					echo '</a>';
				?>
			<?php endif; ?>
		</header>

<?php

	get_template_part( 'loop', 'archive' );
?>


<?php get_footer(); ?>